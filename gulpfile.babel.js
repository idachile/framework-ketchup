'use strict';

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

import gulp from 'gulp';
import pkg from './package.json';
import sftp from 'gulp-sftp';

import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
const browsersync = require('browser-sync').create();
const reload = browserSync.reload;

import gulpLoadPlugins from 'gulp-load-plugins';
const $ = gulpLoadPlugins();

const ssi = require('browsersync-ssi');
const includer = require('gulp-html-ssi');
const ext_replace = require('gulp-ext-replace');
const string_replace = require('gulp-string-replace');
const imagemin = require('gulp-imagemin');
const svgo = require('gulp-svgo');
var rename = require("gulp-rename");
// var htmlbeautify = require('gulp-html-beautify');
const app_path = './app';


// Optimize images
gulp.task('svg', () =>
   gulp.src([
      app_path + '/assets/images/**/*.svg',
   ])
   .pipe($.svgo())
   .pipe(gulp.dest(app_path + '/dist/img'))
   .pipe($.size({
      title: 'svg'
   }))
);
// Optimize images
gulp.task('images', ['svg'], () =>
   gulp.src([
      '!' + app_path + '/assets/images/**/*.svg',
      app_path + '/assets/images/**/*',
   ])
   .pipe($.imagemin())
   .pipe(gulp.dest(app_path + '/dist/img'))
   .pipe($.size({
      title: 'images'
   }))
);

gulp.task('styles', () => {
   const AUTOPREFIXER_BROWSERS = [
      'ie >= 10',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
   ];

   // For best performance, don't add Sass partials to `gulp.src`
   return gulp.src([
         app_path + '/assets/styles/main.scss',
      ])
      .pipe($.sourcemaps.init())
      .pipe($.sass({
         precision: 10
      }).on('error', $.sass.logError))
      .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS, {
         grid: true
      }))
      .pipe($.if('*.css', $.cssnano({
         discardComments: {
            removeAll: true,
         },
         reduceIdents: {
            keyframes: false
         }
      })))
      .pipe($.size({
         title: 'styles'
      }))
      .pipe($.rename({
         suffix: '.min'
      }))
      .pipe($.sourcemaps.write('./'))
      .pipe(gulp.dest(app_path + '/dist/css'));
});


//Styles watch
gulp.task('styles:watch', ['styles'], () => {
   gulp.watch(app_path + '/assets/styles/**/*', ['styles']);
});

//scripts a minificar y aminorar peso sin la necesidad de concatenar
gulp.task('minify', () =>
   gulp.src([
      app_path + '/assets/scripts/minify/*.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.rename({
      suffix: '.min'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/minify'))
);

//scripts que no necesitan minificación ni concatenación y solo se trata de aminorar el peso
gulp.task('support', () =>
   gulp.src([
      app_path + '/assets/scripts/support/*.js',
   ])
   .pipe($.size({
      title: 'support'
   }))
   .pipe(gulp.dest(app_path + '/dist/js/support'))
);

gulp.task('scripts', ['support', 'minify'], () =>
   gulp.src([
      app_path + '/assets/scripts/libs/*.js',
      app_path + '/assets/scripts/src/*.js',
      app_path + '/assets/scripts/*.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('main.min.js'))
   .pipe($.uglify({
      compress: true
   }))
   // Output files
   .pipe($.size({
      title: 'scripts'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js'))
);
//Styles watch
gulp.task('scripts:watch', ['scripts'], () => {
   gulp.watch(app_path + '/assets/scripts/**/*', ['scripts']);
});


gulp.task('modal_slider', () =>
   gulp.src([
      app_path + '/assets/scripts/support/jquery-3.4.1.min.js',
      app_path + '/assets/scripts/libs/ninjaSlider-2.0.js',
      app_path + '/assets/scripts/modal/slider.js',
   ])
   // .pipe($.sourcemaps.init())
   // .pipe($.babel())
   .pipe($.concat('slider.min.js'))
   // .pipe($.uglify({
   //    compress: true
   // }))
   // Output files
   .pipe($.size({
      title: 'scripts'
   }))
   // .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/modal'))
);

gulp.task('watch_this', ['scripts'], () => {
   gulp.watch([app_path + '/**/*.html'], reload);
   gulp.watch([app_path + '/assets/styles/**/**/*.{scss,css}'], ['styles', reload]);
});

//Concatenate and convert shtml to HTML files
//(include .html links)
gulp.task('shtml', () => {
   gulp.src(app_path + '/**/**/*.shtml')
      .pipe(includer())
      .pipe(ext_replace('.html'))
      .pipe(string_replace('.shtml', '.html'))
      // .pipe(string_replace('http://', 'https://'))
      .pipe(gulp.dest('./front'));
});


gulp.task('copy', () => {
   gulp.src([
         app_path + '/dist/**/*',
         // app_path + '/partials/**/*',
      ], {
         base: 'app'
      })
      .pipe(gulp.dest('./front'));
});

gulp.task('front', ['copy', 'shtml'], () => {});



/*************************** */
/*************************** */
/****Servers****** */
/*************************** */
/*************************** */



// Watch files for changes & reload
gulp.task('serve', ['scripts', 'styles', 'images'], () => {
   browserSync({
      notify: false,
      logPrefix: '**WSK**',
      scrollElementMapping: ['main', '.mdl-layout'],
      // https: true,
      // browser: 'firefox nightly',
      browser: 'google chrome',
      server: ['app'],
      port: 3004
   });

   gulp.watch([app_path + '/**/*.html'], reload);
   gulp.watch([app_path + '/assets/styles/**/**/*.{scss,css}'], ['styles', reload]);
   gulp.watch([app_path + '/assets/scripts/libs/*.js', app_path + '/assets/scripts/src/*.js'], ['scripts', reload]);
   gulp.watch([app_path + '/assets/images/**/*'], ['images', reload]);
});

gulp.task('watch', ['front', 'watch_this'], () => {});


//Watch using shtml
// gulp.task('virtual', ['styles', 'scripts', 'images'], () => {
gulp.task('virtual', ['styles', 'scripts'], () => {
   browserSync({
      middleware: ssi({
         baseDir: 'app/',
         baseUrl: `http://localhost:3004`,
         ext: '.shtml',
         version: '1.4.0'
      }),
      notify: false,
      logPrefix: '**WSK**',
      server: ['app'],
      port: 3009,
      https: false,
      browser: "google chrome"
   });
   gulp.watch([app_path + '/**/*.shtml'], reload);
   gulp.watch([app_path + '/assets/styles/**/**/*.{scss,css}'], ['styles', reload]);
   gulp.watch([app_path + '/assets/scripts/libs/*.js', app_path + '/assets/scripts/src/*.js', app_path + '/assets/scripts/minify/*.js', app_path + '/assets/scripts/support/*.js', app_path + '/assets/scripts/*.js'], ['scripts', reload]);
});
gulp.task('compile', ['styles', 'scripts', 'images', ], () => {});

gulp.task('front', ['copy', 'shtml'], () => {});

gulp.task('beauty', () => {
   gulp.src('./front/*.html')
      .pipe(htmlbeautify({
         indent: 3,
         "indent_with_tabs": true,
         "preserve_newlines": false
      }))
      .pipe(gulp.dest('./front/'))
});