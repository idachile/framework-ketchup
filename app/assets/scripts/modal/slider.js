//////////////////////
////////////////////// Common Slider
//////////////////////
(function(window, document, $){
	"use strict";

	// check for NinjaSlider dependency
	if( typeof NinjaSlider === 'undefined' ){
		console.log('Error: NinjaSlider is required');
		return;
	}

	var CommonSlider = function( element ){
		this.container = element;
		this.$container = $($(element).parents('[data-role="slider-parent"]')).length !== 0 ? $($(element).parents('[data-role="slider-parent"]')) : $(element).parent();
		this.$slides = this.$container.find('[data-role="slider-slide"]');
		this.$arrows = this.$container.find('[data-role="slider-arrow"]');
		this.$bullets = this.$container.find('[data-role="slider-bullet"]');
		this.$thumbnails = this.$container.find('[data-role="slider-thumbnail"]');
		this.$captions = this.$container.find('[data-role="slider-caption"]');
		this.$transition = $(element).data('transition') || false;
		this.$speed = $(element).data('speed') || 400;
		this.$autotime = 2000;
		this.$slidelist = this.$container.find('[data-role="slider-list"]');

		if(typeof this.$transition !== 'undefined'){
			this.$autotime = this.$transition;
		}

		console.log();

		this.$slidelist.find('.slider__slide:first-child').addClass('active').attr('data-current', true);

		this.ninjaSlider = new NinjaSlider(this.container, {
			auto: this.$autotime,
			speed: this.$speed,
			transitionCallback: this.transitionCallback.bind(this)
		});

		this.$arrows.on('click.CommonSlider', this.arrowCallback.bind(this));
		this.$captions.on('click.CommonSlider', this.captionCallback.bind(this));
		this.$bullets.on('click.CommonSlider', this.bulletCallback.bind(this));
		this.$thumbnails.on('click.CommonSlider', this.bulletCallback.bind(this));
	};
	CommonSlider.prototype = {
		transitionCallback : function( index, slide, ninjaSlider ){
			if( this.$bullets.length ){
				this.$bullets.removeClass('active');
				this.$bullets.filter('[data-target="'+ index +'"]').addClass('active');
			}	

			if( this.$thumbnails.length ){
				this.$thumbnails.removeClass('active');
				this.$thumbnails.filter('[data-target="'+ index +'"]').addClass('active');
			}		

			if( this.$captions.length ){
				this.$captions.removeClass('active');
				this.$captions.filter('[data-target="'+ index +'"]').addClass('active');
			}

			if( this.$slides.length ){
				this.$slides.removeClass('active');
				this.$slides.filter('[data-index="'+ index +'"]').addClass('active');
			}
		},

		arrowCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				direction = $btn.data('direction');

			this.ninjaSlider.reset();
			this.ninjaSlider[ direction ]();
		},

		captionCallback : function( event ){
			// event.preventDefault();
			var $btn = $(event.currentTarget),
				target = $btn.data('target');
			this.ninjaSlider.reset();
			this.ninjaSlider.slide( target );
		},

		bulletCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				target = $btn.data('target');

			this.ninjaSlider.reset();
			this.ninjaSlider.slide( target );
		}
	};

	$.fn.commonSlider = function(){
		if( this.data('commonSlider') ){ return this.data('commonSlider'); }
		return this.each(function(i, el){
			$(el).data('commonSlider', (new CommonSlider(el)));
		});
	};

	// self initialization
	$(document).ready(function(){
		$('[data-module="modal-slider"]').commonSlider();
	});
}(window, document, jQuery));