$(document).ready(function () {
	jQuery.each(jQuery('[data-touch]'), function (e) {
		var $this = $(this);
		var status = $this.data('touch');
		var $window = $(window);
      if ($window.width() <= 800) {
			$this.removeClass('no-touch').addClass('touch');
			$this.attr('data-touch', true);
		}else{
			$this.addClass('no-touch').removeClass('touch');
			$this.attr('data-touch', false);
		}
	});
});